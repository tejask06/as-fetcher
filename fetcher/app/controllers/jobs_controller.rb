class JobsController < ApplicationController
    def index
        @jobs=Delayed::Job.paginate({:page => params[:page]})
        resp=[]
        @jobs.each do |job|
            resp << {job: job, request: job.payload_object.request}
        end
        render json: {result: resp, total_pages: @jobs.total_pages, current_page: @jobs.current_page}
    end
end
