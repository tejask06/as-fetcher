class BooksController < ApplicationController
    def search
        @books=Book.where("isbn in (:isbns) or ean in (:isbns)",{:isbns => book_search_params[:q].split(",").each {|isbn| isbn.strip!}})
        render json: @books
    end

    def download_all
        @books=Book.all
        send_data(@books.to_csv, :filename => "books-#{DateTime.now}.csv")
    end

    def delete_all
        Book.destroy_all
        render json: {:message => "Done"}
    end

    private
    def book_search_params
        params.require(:q)
    end
end
