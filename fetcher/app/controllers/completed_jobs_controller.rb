class CompletedJobsController < ApplicationController
	before_action :authenticate_user!
    before_action :set_completed_job, only: [:download_error_isbns]

	def index
		@completed_jobs = CompletedJob.paginate({:page => params[:page] || 1})
		resp=[]
		@completed_jobs.each do |job|
			resp << {job: job, request: if FetchRequest.exists? job.request_id then FetchRequest.find(job.request_id) else nil end }
		end
		render json: {result: resp, total_pages: @completed_jobs.total_pages, current_page: @completed_jobs.current_page}
	end

    def delete_all
        CompletedJob.destroy_all
        render json: {:message => "Done"}
    end

    def download_error_isbns
    	file_name="/tmp/failure_ids-#{DateTime.now}.csv"
    	f=File.new(file_name, "w")
    	@completed_job.failure_ids.each { |id| f.write("#{id}\n") }
    	f.close
        send_file(file_name, :type => "application/csv", :status=>200)
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_completed_job
        @completed_job = CompletedJob.find(params[:id])
    end
end
