class FetchRequestsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_fetch_request, only: [:show, :edit, :update, :destroy, :download_source]
    skip_before_filter :verify_authenticity_token
    # GET /fetch_requests
    # GET /fetch_requests.json
    def index
        fetch_requests = FetchRequest.paginate({:page => params[:page]})
        render json: {result: fetch_requests, total_pages: fetch_requests.total_pages, current_page: fetch_requests.current_page}
    end

    def create
        uploaded_io = params[:file]
        file_name = Rails.root.join('public', 'uploads', uploaded_io.original_filename)
        File.open(file_name, 'wb') do |file|
            file.write(uploaded_io.read)
        end

        @amazon_fetch_request = FetchRequest.new({:site => "Amazon",:file_name => file_name, :requestor => current_user.id})
        # @shimply_fetch_request = FetchRequest.new({:site => "Shimply",:file_name => file_name, :requestor => current_user.id})

        if @amazon_fetch_request.save #&& @shimply_fetch_request.save
            render json: @fetch_request
        else
            render json: @amazon_fetch_request.errors, status: :unprocessable_entity
        end
    end

    def download_source
        send_file(@fetch_request.file_name, :type => "application/csv",:status=>200)
    end

    def delete_all
        FetchRequest.all.each do |request|
            FileUtils.rm_f request.file_name
            request.destroy
        end
        render json: {:message => "Done"}
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_fetch_request
        @fetch_request = FetchRequest.find(params[:id])
    end
end
