class FetchJob < ProgressJob::Base
    include AmazonHelper
    include ShimplyHelper

    @@logger = Logger.new(File.join(Rails.root, 'log', 'fetch_job.log'))

    def initialize(fetch_req_id)
        @fetch_req_id=fetch_req_id
        @failure_ids = []
    end

    def request_id
        @fetch_req_id
    end

    def request
        req = nil
        begin
            req = FetchRequest.find(@fetch_req_id)
        rescue ActiveRecord::RecordNotFound => ex
            @@logger.error("Request with id #{@fetch_req_id} not found")
        end
        req
    end

    def failure_ids
        @failure_ids
    end

    def perform
        @start_time=Time.now
        if request.present?
            isbns = if File.exist? request.file_name then File.readlines(request.file_name) else [] end
            update_progress_max(isbns.length)
            isbns.each do |isbn|
                isbn = isbn.strip!
                begin
                    if request.site == "Amazon"
                        book=Book.from_amazon_response AmazonHelper.get_book(isbn), isbn
                    else
                        book=Book.from_shimply_response ShimplyHelper.get_book(isbn)[isbn], isbn
                    end
                    book.save!
                    @@logger.info("#{request.site}|#{isbn}|#{book}|")
                rescue Exception => e
                    @failure_ids << isbn
                    @@logger.error("Error in ISBN #{isbn} and site #{request.site}")
                    @@logger.error(e)
                end
                update_progress
            end
            @@logger.info("Failure ids: #{@failure_ids}") if @failure_ids.size > 0
        end
        @end_time=Time.now
    end

    def start_time
        @start_time
    end

    def end_time
        @end_time
    end

    def success job
        completed_job=CompletedJob.new({:job_id => job.id, :request_id => job.payload_object.request_id , :failure_ids => job.payload_object.failure_ids, :start_time => job.payload_object.start_time, :end_time => job.payload_object.end_time})
        completed_job.save
    end
end
