# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

Array::getUnique = ->
	u = {}
	a = []
	i = 0
	l = @length
	while i < l
		if u.hasOwnProperty(@[i])
			++i
			continue
		a.push @[i]
		u[@[i]] = 1
		++i
	a

fetcher=angular.module('fetcher',['ngFileUpload'])

fetcher.controller 'RunningJobsCtrl', ['$scope', '$http', '$rootScope', ($scope, $http, $rootScope) ->
	$scope.refreshFlag=true
	$rootScope.$on("ReloadRunningJobs", () -> 
		$scope.refresh()
	)

	$scope.refresh= ()->
		$scope.refreshFlag=!$scope.refreshFlag
]

fetcher.controller 'CompletedJobsCtrl', ['$scope', '$http', '$rootScope', ($scope, $http, $rootScope) ->
	$scope.refreshFlag=true
	$rootScope.$on("ReloadCompletedJobs", () -> 
		$scope.refresh()
	)

	$scope.deleteAll=->
		$("#collapseThree").LoadingOverlay 'show'
		$http.get("/completed_jobs/delete_all").then (response) ->
			$("#collapseThree").LoadingOverlay 'hide'
			$rootScope.$emit("ReloadCompletedJobs", {})

	$scope.getFormattedDateTime=(dateTimeString) ->
		formattedDate=new Date(dateTimeString)
		(formattedDate.getMonth() + 1) + "-" + formattedDate.getDate() + "-" + formattedDate.getFullYear() + " " + formattedDate.getHours() + ":" + formattedDate.getMinutes() + ":" + formattedDate.getSeconds()

	$scope.refresh=() ->
		$scope.refreshFlag=!$scope.refreshFlag
]

fetcher.controller 'RequestsCtrl', ['$scope', 'Upload', '$http', '$rootScope', ($scope, Upload, $http, $rootScope) -> 
	$scope.refreshFlag=true
	$scope.errors=[]

	$rootScope.$on("ReloadFetchRequests", () -> 
		$scope.refresh()
	)

	$scope.refresh=() ->
		$scope.refreshFlag=!$scope.refreshFlag

	$scope.deleteAll=->
		$("#collapseOne").LoadingOverlay 'show'
		$http.get("/fetch_requests/delete_all").then (response) ->
			$("#collapseOne").LoadingOverlay 'hide'
			$rootScope.$emit("ReloadFetchRequests", {})

	$scope.submitForm= ->
		Upload.upload(
			url: 'fetch_requests/create.json'
			data:
				file: $scope.file
		).then ((resp) ->
			$("#addRequestModal").modal("hide")
			$scope.refreshFlag=!$scope.refreshFlag
			$rootScope.$emit("ReloadRunningJobs", {})
			console.log 'Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data
			return
		), ((resp) ->
			$scope.errors = [].concat(values(response.data)...)
			console.log 'Error status: ' + resp.status
			return
		), (evt) ->
			progressPercentage = parseInt(100.0 * evt.loaded / evt.total)
			console.log 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name
			return
	$scope.clearForm= -> 
		$scope.request = {}
		$scope.errors = []
		return
	$scope.sites=["Amazon","Shimply"]
]

fetcher.controller 'BooksCtrl', ['$scope', '$http', '$rootScope', ($scope, $http, $rootScope) ->
	$scope.findBook=(isbn)->
		$("#collapseFour").LoadingOverlay 'show'
		$http.get("/books/search?q=" + isbn).then (response) ->
			$scope.books = response.data
			$("#collapseFour").LoadingOverlay 'hide'

	$scope.deleteAll=->
		$("#collapseFour").LoadingOverlay 'show'
		$http.get("/books/delete_all").then (response) ->
			$("#collapseFour").LoadingOverlay 'hide'


	$scope.getFormattedDateTime=(dateTimeString) ->
		formattedDate=new Date(dateTimeString)
		(formattedDate.getMonth() + 1) + "-" + formattedDate.getDate() + "-" + formattedDate.getFullYear() + " " + formattedDate.getHours() + ":" + formattedDate.getMinutes() + ":" + formattedDate.getSeconds()

]


fetcher.directive 'progressBar', ['$http', '$timeout', '$rootScope', ($http, $timeout, $rootScope) ->
	restrict: "E"
	scope:
		jobId: "="
	templateUrl: "progress-bar.html"
	link: (scope, elem, attrs) ->
		timer=null
		(tick=()->
			$http.get("progress-job/"+scope.jobId).then((response) ->
					scope.percentage=response.data.percentage
					timer=$timeout tick, 1000
				, (response) ->
					scope.percentage=100
					$rootScope.$emit("ReloadCompletedJobs", {})
					$rootScope.$emit("ReloadFetchRequests", {})
					$rootScope.$emit("ReloadRunningJobs", {})
					$timeout.cancel(timer)
				)
		)()
]

fetcher.directive 'pagination', ['$http', ($http) ->
	restrict: "E"
	scope:
		collection: "="
		baseUrl:"="
		refFlag:"="
		collapseId: "="
	templateUrl: "pagination.html"
	link: (scope, elem, attrs) ->
		scope.$watch 'refFlag', (newValue, oldValue) ->
			scope.go(scope.currentPage)
			return
		scope.getPages=->
			[1..scope.totalPages]
		scope.go=(page)->
			$("#"+scope.collapseId).LoadingOverlay 'show'
			page= if page then "?page=" + page else ""
			$http.get(scope.baseUrl + page).then (response) ->
				scope.collection = response.data.result
				scope.currentPage = response.data.current_page
				scope.totalPages = response.data.total_pages
				$("#"+scope.collapseId).LoadingOverlay 'hide'
		scope.go()
]