json.extract! fetch_request, :id, :site, :ids, :requestor, :created_at, :updated_at
json.url fetch_request_url(fetch_request, format: :json)