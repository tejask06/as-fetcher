require 'net/http'
require 'json'
module ShimplyHelper
    @@api_key = "0709c8ec890aa56b178311b5101ac64b"
    @@base_url = "https://www.shimply.com/api/search/get"

    def self.get_book isbn
        uri = URI(@@base_url)
        params = { :key => @@api_key, :isbn => isbn }
        uri.query = URI.encode_www_form(params)

        res = Net::HTTP.get_response(uri)
        resp = JSON.parse(res.body)
        if resp[:error].present? || resp["error"].present?
            raise Exception.new "Book Not found"
        else
            resp
        end
    end
end
