require 'amazon/ecs'
require 'time'
require 'uri'
require 'openssl'
require 'base64'

module AmazonHelper
    @@AWS_ACCESS_KEY_ID = "AKIAIZCD5NMYFUGKF3GA"
    @@AWS_SECRET_KEY = "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"
    @@ENDPOINT = "webservices.amazon.in"
    @@REQUEST_URI = "/onca/xml"

    @@params = {
        "Service" => "AWSECommerceService",
        "Operation" => "ItemLookup",
        "AWSAccessKeyId" => @@AWS_ACCESS_KEY_ID,
        "AssociateTag" => "thtemo-21",
        "IdType" => "ISBN",
        "ResponseGroup" => "Images,ItemAttributes,Offers",
        "SearchIndex" => "Books"
    }

    def self.get_book isbn
        @@params["Timestamp"] = Time.now.gmtime.iso8601 if !@@params.key?("Timestamp")
        @@params["ItemId"] = isbn

        # Generate the canonical query
        canonical_query_string = @@params.sort.collect do |key, value|
            [URI.escape(key.to_s, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")), URI.escape(value.to_s, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))].join('=')
        end.join('&')

        string_to_sign = "GET\n#{@@ENDPOINT}\n#{@@REQUEST_URI}\n#{canonical_query_string}"

        signature = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), @@AWS_SECRET_KEY, string_to_sign)).strip()

        request_url = "http://#{@@ENDPOINT}#{@@REQUEST_URI}?#{canonical_query_string}&Signature=#{URI.escape(signature, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))}"

        uri = URI(request_url)
        res = Net::HTTP.get_response(uri)

        response=Nokogiri::XML(res.body)
        if response.at_css("Items > Request > Errors > Error > Message").present?
            raise Exception.new response.at_css("Items > Request > Errors > Error > Message").text
        elsif response.at_css("ItemLookupErrorResponse > Error > Message").present?
            raise Exception.new response.at_css("ItemLookupErrorResponse > Error > Message").text
        else
            response
        end
    end
end
