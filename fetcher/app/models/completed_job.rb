class CompletedJob < ApplicationRecord
    serialize :failure_ids, JSON
end
