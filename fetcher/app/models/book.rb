require 'csv'
class Book < ApplicationRecord
    def self.to_csv
        attributes = %w{request_id site author availability binding ean image isbn language link num_pages price publisher year title}

        CSV.generate(headers: true) do |csv|
            csv << attributes
            all.each do |user|
                csv << attributes.map{ |attr| user.send(attr) }
            end
        end
    end

    def self.from_amazon_response xml, isbn
        book=Book.new
        book.request_id=isbn
        book.site= "Amazon"
        availability=xml.at_css("Items > Item > Offers > Offer > OfferListing > AvailabilityAttributes > AvailabilityType").text if  xml.at_css("Items > Item > Offers > Offer > OfferListing > AvailabilityAttributes > AvailabilityType").present?
        book.availability= if availability == "now" then "in_stock" else "out_of_stock" end
        book.author= xml.at_css("Items > Item > ItemAttributes > Author").text if  xml.at_css("Items > Item > ItemAttributes > Author").present?
        book.binding= xml.at_css("Items > Item > ItemAttributes > Binding").text if xml.at_css("Items > Item > ItemAttributes > Binding").present?
        book.ean= xml.at_css("Items > Item > ItemAttributes > EAN").text if xml.at_css("Items > Item > ItemAttributes > EAN").present?
        book.image= xml.at_css("Items > Item > LargeImage > URL").text if xml.at_css("Items > Item > LargeImage > URL").present?
        book.isbn= xml.at_css("Items > Item > ItemAttributes > ISBN").text if xml.at_css("Items > Item > ItemAttributes > ISBN").present?
        book.language= xml.at_css("Items > Item > ItemAttributes > Languages > Language > Name").text if xml.at_css("Items > Item > ItemAttributes > Languages > Language > Name").present?
        book.link= xml.at_css("Items > Item > DetailPageURL").text if xml.at_css("Items > Item > DetailPageURL").present?
        book.num_pages= xml.at_css("Items > Item > ItemAttributes > NumberOfPages").text if  xml.at_css("Items > Item > ItemAttributes > NumberOfPages").present?
        book.price= xml.at_css("Items > Item > ItemAttributes > ListPrice > FormattedPrice").text if  xml.at_css("Items > Item > ItemAttributes > ListPrice > FormattedPrice").present?
        book.publisher= xml.at_css("Items > Item > ItemAttributes > Publisher").text if  xml.at_css("Items > Item > ItemAttributes > Publisher").present?
        book.year= xml.at_css("Items > Item > ItemAttributes > PublicationDate").text if  xml.at_css("Items > Item > ItemAttributes > PublicationDate").present?
        book.title= xml.at_css("Items > Item > ItemAttributes > Title").text if  xml.at_css("Items > Item > ItemAttributes > Title").present?
        book
    end

    def self.from_shimply_response json, isbn
        book=Book.new
        book.request_id=isbn
        book.site= "Shimply"
        book.title= json["name"]
        book.price= json["price"]
        book.image= json["image_url"]
        book.availability= if (json["instock"] == "0") then "out_of_stock" else "in_stock" end
        book.link= json["permalink"]
        book.price= json["price"]
        book
    end
end
