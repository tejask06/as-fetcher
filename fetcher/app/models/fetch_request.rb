class FetchRequest < ApplicationRecord
    after_save :create_job

    private
    def create_job
        Delayed::Job.enqueue FetchJob.new self.id
    end
end
