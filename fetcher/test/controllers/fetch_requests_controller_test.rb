require 'test_helper'

class FetchRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fetch_request = fetch_requests(:one)
  end

  test "should get index" do
    get fetch_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_fetch_request_url
    assert_response :success
  end

  test "should create fetch_request" do
    assert_difference('FetchRequest.count') do
      post fetch_requests_url, params: { fetch_request: { ids: @fetch_request.ids, requestor: @fetch_request.requestor, site: @fetch_request.site } }
    end

    assert_redirected_to fetch_request_url(FetchRequest.last)
  end

  test "should show fetch_request" do
    get fetch_request_url(@fetch_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_fetch_request_url(@fetch_request)
    assert_response :success
  end

  test "should update fetch_request" do
    patch fetch_request_url(@fetch_request), params: { fetch_request: { ids: @fetch_request.ids, requestor: @fetch_request.requestor, site: @fetch_request.site } }
    assert_redirected_to fetch_request_url(@fetch_request)
  end

  test "should destroy fetch_request" do
    assert_difference('FetchRequest.count', -1) do
      delete fetch_request_url(@fetch_request)
    end

    assert_redirected_to fetch_requests_url
  end
end
