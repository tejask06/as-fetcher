Rails.application.routes.draw do
    devise_for :users
    get 'welcome/index'
    root to: 'welcome#index'

    match ':controller/:action/:id', via: [:get, :post]
    match ':controller/:action', via: [:get, :post]
    match ':controller', action: "index", via: [:get, :post]
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
