class CreateFetchRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :fetch_requests do |t|
      t.string :site
      t.string :ids
      t.integer :requestor

      t.timestamps
    end
  end
end
