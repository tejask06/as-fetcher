class CreateCompletedJobs < ActiveRecord::Migration[5.0]
    def change
        create_table :completed_jobs do |t|
            t.integer :job_id
            t.integer :request_id
            t.text :failure_ids

            t.timestamps
        end
    end
end
