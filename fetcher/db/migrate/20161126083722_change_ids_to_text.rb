class ChangeIdsToText < ActiveRecord::Migration[5.0]
    def change
        change_column :fetch_requests, :ids, :text
    end
end
