class RenameTimeTakenAddEndTime < ActiveRecord::Migration[5.0]
    def change
        rename_column :completed_jobs, :time_taken, :start_time
        change_column :completed_jobs, :start_time, :datetime
        add_column :completed_jobs, :end_time, :datetime
    end
end
