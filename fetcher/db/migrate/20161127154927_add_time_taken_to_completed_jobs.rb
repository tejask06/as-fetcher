class AddTimeTakenToCompletedJobs < ActiveRecord::Migration[5.0]
    def change
        add_column :completed_jobs, :time_taken, :string
    end
end
