class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :author
      t.string :availability
      t.string :binding
      t.string :ean
      t.string :image
      t.string :isbn
      t.string :language
      t.string :link
      t.string :num_pages
      t.string :price
      t.string :publisher
      t.string :year
      t.string :reading_level
      t.string :category
      t.string :sub_category
      t.string :title

      t.timestamps
    end
  end
end
