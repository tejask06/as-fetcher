class ChangeIdsToFileName < ActiveRecord::Migration[5.0]
    def change
        rename_column :fetch_requests, :ids, :file_name
    end
end
